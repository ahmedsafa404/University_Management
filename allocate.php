<?php
include ("vendor/autoload.php");
use Author\Admin\Admin;

if($_POST)
{
    $department = $_POST['department'];
    $course = $_POST['course'];
    $room = $_POST['room'];
    $day = $_POST['day'];
    $from = $_POST['from'];
    $to = $_POST['to'];
    $starTformat= $_POST['startTime'];
    $endFromat = $_POST['endTime'];

    $starTam = 0;
    $starTpm = 0;
    $endAm = 0;
    $endPm = 0;
    if($starTformat == "AM")
    {
        $starTam = 1;

    }
    elseif ($starTformat == "PM")
    {
        $starTpm = 1;

    }
    if($endFromat == "AM")
    {
        $endAm = 1;
    }
    elseif($endFromat == "PM")
    {
        $endPm = 1;
    }

    if($department == "" or $course == "" or $room == "" or $day == "" or $from == "" or $to == "" or $starTformat == "" or $endFromat =="")
    {
        echo "All Field required";
    }

    $allocate = new Admin();
    $allocate->allocate($_POST);

    header("location:admin/classroom.php");
}
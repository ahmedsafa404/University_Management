<?php
include ("vendor/autoload.php");
use Author\Admin\Admin;

if($_POST)
{
    $name = $_POST['name'];
    $email = $_POST['email'];
    $contact = $_POST['contact'];
    $date = $_POST['date'];
    $address = $_POST['address'];
    $department = $_POST['department'];

    //XSS Filter

    $name = strip_tags($name);
    $email = strip_tags($email);
    $contact = strip_tags($contact);
    $date = strip_tags($date);
    $address = strip_tags($address);
    $department = strip_tags($department);

    $name = stripslashes($name);
    $email = stripslashes($email);
    $contact = stripslashes($contact);
    $date = stripslashes($date);
    $address = stripslashes($address);
    $department = stripslashes($department);

    $name = htmlspecialchars($name);
    $email = htmlspecialchars($email);
    $contact = htmlspecialchars($contact);
    $date = htmlspecialchars($date);
    $address = htmlspecialchars($address);
    $department = htmlspecialchars($department);

    if($name == "" or $email == "" or $contact == "" or $date == "" or $address == "" or $department == "")
    {
        echo "All Field Required";
    }
    elseif (!filter_var($email,FILTER_VALIDATE_EMAIL))
    {
        echo "Invalid Email Format";
    }
    $register = new Admin();
    $register->Student($_POST);

    header("location:admin/student.php");


}
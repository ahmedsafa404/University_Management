<?php
include ("vendor/autoload.php");
use Author\Admin\Admin;

if($_POST)
{

    $code = $_POST['code'];
    $name = $_POST['name'];
    $credit = $_POST['credit'];
    $description = $_POST['description'];
    $department = $_POST['department'];
    $semester = $_POST['semester'];

    //XSS Filter

    $code = strip_tags($code);
    $name = strip_tags($name);
    $credit = strip_tags($credit);
    $description = strip_tags($description);
    $department = strip_tags($department);
    $semester = strip_tags($semester);

    $code = stripslashes($code);
    $name = stripslashes($name);
    $credit = stripslashes($credit);
    $description = stripslashes($description);
    $department = stripslashes($department);
    $semester = stripslashes($semester);

    $code = htmlspecialchars($code);
    $name = htmlspecialchars($name);
    $credit = htmlspecialchars($credit);
    $description = htmlspecialchars($description);
    $department = htmlspecialchars($department);
    $semester = htmlspecialchars($semester);


    if($code == "" or $name == "" or $credit == "" or $description == "" or $department == "" or $semester == "")
    {
        echo "<font color='red'>All Field Required</font>";


    }
    elseif(strlen($code) < 5)
    {
        echo "Code must be at least five (5) characters long<br>";
    }
    elseif($credit < 0.5 or $credit > 5)
    {
        echo " Credit cannot be less than 0.5 and more than 5.0";
    }






    $course = new Admin();
    $course->addCourse($_POST);

    header("location:admin/course.php");
}
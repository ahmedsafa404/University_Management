<?php
include ("vendor/autoload.php");
use Author\Admin\Admin;

if($_POST)
{
    $code = strip_tags($_POST['code']);
    $department = strip_tags($_POST['department']);

    $code = stripcslashes($code);
    $department = stripcslashes($department);

    $code = htmlspecialchars($code);
    $department = htmlspecialchars($department);

    if($code == "" or $department == "")
    {
        echo "Both Field Required";
    }
    elseif ($code > 2 and $code < 7 )
    {
        echo "Code must be two (2) to seven (7) characters long.";
    }
    $dept = new Admin();
    $dept->setDepartment($_POST);

    header("location:admin/department.php");
}
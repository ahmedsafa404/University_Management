<?php
namespace Author\Admin;
use PDO;


class Admin
{
    private $username;
    private $password;
    private $code;
    private $course;
    private $name;
    private $address;
    private $email;
    private $endTo;
    private $contact;
    private $credit;
    private $date;
    private $day;
    private $description;
    private $designation;
    private $department;
    private $from;
    private $to;
    private $room;
    private $startFrom;
    private $startTime;
    private $endTime;
    private $semester;


    public function login($info = "")
    {
        $con = new PDO("mysql:host=localhost;dbname=project","root","");

        $this -> username=$info['username'];
        $this -> password=md5($info['password']);

        //Authentication

        $query = "SELECT * FROM admin WHERE username = ? and password = ? LIMIT 1";
        $query = $con->prepare($query);

        $query->bindValue(1,$this->username);
        $query->bindValue(2,$this->password);
        $query->execute();

        if($query->rowCount() == 1 )
        {
            session_start();
            $_SESSION['username'] = $this->username;

            header("Location:admin/index.php");
        }
        else
        {
            echo "Incorrect Username or Password";
        }

    }

    public function setDepartment($info = "")
    {
        $this->code = $info['code'];
        $this->department = $info['department'];

        $con = new PDO("mysql:host=localhost;dbname=project","root","");

        $query = "INSERT INTO department(code,name) VALUES(:c,:n)";

        $query = $con->prepare($query);

        $query->bindValue(':c',$this->code);
        $query->bindValue(':n',$this->department);

        $codeExist = "SELECT code,name FROM department WHERE code = ? and name = ? LIMIT 1";
        $codeExist = $con->prepare($codeExist);

        $codeExist->bindValue(1,$this->code);
        $codeExist->bindValue(2,$this->department);

        $codeExist->execute();

        if($codeExist->rowCount() == 1)
        {
            echo "Code or Name Already Exist";
            return;
        }
        $query->execute();

    }

    public function allDepartment()
    {
        $con = new PDO("mysql:host=localhost;dbname=project","root","");

        $query = "SELECT * FROM department";
        $query = $con->prepare($query);

        $query->execute();

        $department = $query->fetchAll();

        return $department;

//        echo "<pre>";
//        print_r($department);

    }
    public function addCourse($info = "")
    {
        $this->code = $info['code'];
        $this->name = $info['name'];
        $this->credit = $info['credit'];
        $this->description = $info['description'];
        $this->department = $info['department'];
        $this->semester = $info['semester'];

        $con = new PDO("mysql:host=localhost;dbname=project","root","");

        $query = "INSERT INTO courses(code,name,credit,description,department,semester) VALUES(:c,:n,:cr,:des,:dep,:s)";

        $query = $con->prepare($query);

        $query->bindValue(':c',$this->code);
        $query->bindValue(':n',$this->name);
        $query->bindValue(':cr',$this->credit);
        $query->bindValue(':des',$this->description);
        $query->bindValue(':dep',$this->department);
        $query->bindValue(':s',$this->semester);

        $Exist = "SELECT code,name FROM courses WHERE code = ? and name = ? LIMIT 1";
        $Exist = $con->prepare($Exist);

        $Exist->bindValue(1,$this->code);
        $Exist->bindValue(2,$this->name);

        $Exist->execute();

        if($Exist->rowCount() == 1)
        {
            echo "Code or Name Already Exist";
            return;
        }
        $query->execute();
        header("location:admin/course.php");

    }
    public function allCourse()
    {
        $con = new PDO("mysql:host=localhost;dbname=project","root","");

        $query = "SELECT * FROM courses";
        $query = $con->prepare($query);

        $query->execute();

        $course = $query->fetchAll();

        return $course;



    }

    public function Designation()
    {
        $con = new PDO("mysql:host=localhost;dbname=project","root","");

        $query = "SELECT * FROM designation";
        $query = $con->prepare($query);

        $query->execute();

        $designation = $query->fetchAll();

        return $designation;

    }

    public function Teacher($info = "")
    {


        $this->name = $info['name'];
        $this->address = $info['address'];
        $this->email = $info['email'];
        $this->contact = $info['contact'];
        $this->designation = $info['designation'];
        $this->department = $info['department'];
        $this->credit = $info['creditBtaken'];

        $con = new PDO("mysql:host=localhost;dbname=project","root","");

        $query = "INSERT INTO teachers(name,address,email,contact,designation,department,credit) VALUES(:n,:a,:e,:c,:des,:dep,:cr)";

        $query = $con->prepare($query);

        $query->bindValue(':n',$this->name);
        $query->bindValue(':a',$this->address);
        $query->bindValue(':e',$this->email);
        $query->bindValue(':c',$this->contact);
        $query->bindValue(':des',$this->designation);
        $query->bindValue(':dep',$this->department);
        $query->bindValue(':cr',$this->credit);

        $codeExist = "SELECT name,email FROM teachers WHERE name = ? and email = ? LIMIT 1";
        $codeExist = $con->prepare($codeExist);

        $codeExist->bindValue(1,$this->name);
        $codeExist->bindValue(2,$this->email);

        $codeExist->execute();

        if($codeExist->rowCount() == 1)
        {
            echo "Name or Email Already Exist";
            return;
        }
        $query->execute();

    }
    public function Student($info = "")
    {


        $this->name = $info['name'];
        $this->email = $info['email'];
        $this->contact = $info['contact'];
        $this->date = $info['date'];
        $this->address = $info['address'];
        $this->department = $info['department'];


        $con = new PDO("mysql:host=localhost;dbname=project","root","");

        $query = "INSERT INTO student(name,email,contact,date,address,department) VALUES(:n,:e,:c,:d,:a,:dep)";

        $query = $con->prepare($query);

        $query->bindValue(':n',$this->name);
        $query->bindValue(':e',$this->email);
        $query->bindValue(':c',$this->contact);
        $query->bindValue(':d',$this->date);
        $query->bindValue(':a',$this->address);
        $query->bindValue(':dep',$this->department);


        $emailExist = "SELECT email FROM student WHERE email = ? LIMIT 1";
        $emailExist = $con->prepare($emailExist);

        $emailExist->bindValue(1,$this->email);


        $emailExist->execute();

        if($emailExist->rowCount() == 1)
        {
            echo "Email Already Exist";
            return;
        }
        $query->execute();

    }

    public function room()
    {
        $con = new PDO("mysql:host=localhost;dbname=project","root","");

        $query = "SELECT * FROM class";
        $query = $con->prepare($query);

        $query->execute();

        $room = $query->fetchAll();

        return $room;
    }
    public function allocate($info = "")
    {
        $this->department = $info['department'];
        $this->course = $info['course'];
        $this->room = $info['room'];
        $this->day = $info['day'];
        $this->from = $info['from'];
        $this->to = $info['to'];
        $this->startFrom = $info['startTime'];
        $this->endTo = $info['endTime'];
        $con = new PDO("mysql:host=localhost;dbname=project","root","");

        $query = "INSERT INTO class_schedule(department,course,room,day,startTime,startFrom,endTime,endTo)VALUES (:dep,:c,:r,:d,:st,:s,:et,:e)";
        $query = $con->prepare($query);

        $query->bindValue(':dep',$this->department);
        $query->bindValue(':c',$this->course);
        $query->bindValue(':r',$this->room);
        $query->bindValue(':d',$this->day);
        $query->bindValue(':st',$this->from);
        $query->bindValue(':s',$this->startFrom);
        $query->bindValue(':et',$this->to);
        $query->bindValue(':e',$this->endTo);

        $timeCheck = "SELECT room,startFrom,endTO FROM class_schedule WHERE room = ? OR startFrom = ? OR endTo = ? LIMIT 1";
        $timeCheck = $con->prepare($timeCheck);

        $timeCheck->bindValue(1,$this->room);
        $timeCheck->bindValue(1,$this->startFrom);
        $timeCheck->bindValue(2,$this->endTo);

        $timeCheck->execute();

        if($timeCheck->rowCount() == 1 )
        {
            echo "This Time Already Allocated ";
            return;
        }

        $query->execute();

    }


}
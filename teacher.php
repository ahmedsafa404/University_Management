<?php
include ("vendor/autoload.php");
use Author\Admin\Admin;


if($_POST)
{
    $name = $_POST['name'];
    $address = $_POST['address'];
    $email = $_POST['email'];
    $contact = $_POST['contact'];
    $designation = $_POST['designation'];
    $department = $_POST['department'];
    $credit = $_POST['creditBtaken'];

    //XSS Filter
    $name = strip_tags($name);
    $address = strip_tags($address);
    $email = strip_tags($email);
    $contact = strip_tags($contact);
    $designation = strip_tags($designation);
    $department = strip_tags($department);
    $credit = strip_tags($credit);

    $name = stripslashes($name);
    $address = stripslashes($address);
    $email = stripslashes($email);
    $contact = stripslashes($contact);
    $designation = stripslashes($designation);
    $department = stripslashes($department);
    $credit = stripslashes($credit);

    $name = htmlspecialchars($name);
    $address = htmlspecialchars($address);
    $email = htmlspecialchars($email);
    $contact = htmlspecialchars($contact);
    $designation = htmlspecialchars($designation);
    $department = htmlspecialchars($department);
    $credit = htmlspecialchars($credit);

    if($name == "" or $address == "" or $email == "" or $contact =="" or $designation == "" or $department== "" or $credit =="" )
    {
        echo "<font color='red'>All Field Required!</font>";
    }
    elseif (!filter_var($email,FILTER_VALIDATE_EMAIL))
    {
        echo "Invalid E-mail Format!";
    }
    elseif ($credit < 0)
    {
        echo "Credit to be taken field must contain a non-negative value.";
    }

    $teacher = new Admin();
    $teacher->Teacher($_POST);

    header("location:admin/teacher.php");

}
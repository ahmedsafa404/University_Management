<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit9d80735d1816f8d56586901b7198d8a5
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'Author\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Author\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit9d80735d1816f8d56586901b7198d8a5::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit9d80735d1816f8d56586901b7198d8a5::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
